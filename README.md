# README #

This README would normally document whatever steps are necessary to get your application up and running.

Este documento es la guia de instalación y configuracion para el proyecto. A continuacion encontrara una rapida descripcion de los componentes utilizados asi como de la forma como deben ser instalados para su uso.

Componentes e instalacion
Sitema operativo
Esta montado en sistemas operativos Linux; probado en ubuntu 18.04. Con conocimiento en sistemas operativos windows su instalacion y configuracion no debera tener ningun problema.

Ambiente virtual
El proyecto debe residir dentro de ambiente virtual especificamente para su uso.El ambiente virtual debe estar montado bajo:

Pythom 3.6 +
Base de datos
Respecto al tema de la base de datos se ha seleccionado el motor de base de datos relacional postgresql.
Postgresql 9.6 +
Framework de desarrollo
Respecto al framework de desarrollo se cuenta con django. La version 2.2

Librerias
Ademas del framework de desarrollo web, se implemento una serie de librerias las cuales cumplen diferentes funciones dentro de la aplicacion. Para la instalacion de estas librerias lo haremos mediante el uso del archivo requirements.txt el cual se encuentra en la raiz del proyecto.

$ pip install requirements.txt

Es necesario que cuando se implemente una nueva libreria se vuelva a generar el archivo y este sea actualizado en el repositorio. Para generar este archivo corremos el siguiente comando:

$ pip freeze > requirements.txt

Variables de entorno
Estas variables son utilizadas para asegurar la independecia de los ambientes virtuales, asegurar las credecianles de desarrollo y organizar todas las costantes utlizadas dentro del proyecto.

Estas variables deben ser declaradas dentro del archivo .env 

Creacion de la Base de datos 

Para la creación de la base de datos tendremos que abrir una nueva terminal en la que vamos a seguir los siguientes comandos:

$ sudo su postgres
$ psql
$ create database test;
$ CREATE USER test PASSWORD 'test';
$ ALTER DATABASE test owner to test;

Una vez hecho esto procedemos a migrar la base de datos con el comando:

$ python manage.py migrate

Luego de Esto ejecutamos el comando:

$python manage.py createuser 

Creamos un nuevo usuario con las siguientes credenciales:

email:
admin@gmail.com

Contraseña:
Admin12345

Luego de esto procedemos a ingresar a iniciar el servidor con el comando:

$ python manage.py runserver

Una vez el servidor este arriba ingresamos al aplicativo mediante la url:

http://127.0.0.1:8000/login/

Ingresamos al aplicativo con el usuario:

admin@gmail.com

Contraseña:

Admin12345

Una vez iniciemos sesion encontraremos las diferentes operaciones del sistema