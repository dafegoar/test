from django import forms
from files.models import File

class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = [ 'document']