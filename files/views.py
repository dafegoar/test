from django.shortcuts import render, redirect, get_object_or_404
from files.forms import FileForm
from files.models import *
from files.serializers import *
from boto3.session import Session
from rest_framework import generics
import pandas as pd
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
import logging
import boto3
from botocore.exceptions import ClientError

# Create your views here.
class FilesList(generics.ListCreateAPIView):

    queryset = File.objects.all()
    S3_ACCESS_KEY = 'BAKIBAKI678H67HGA'
    S3_SECRET_KEY = '+vpOpILD+E9872AialendX0Ui123CKCKCKw'
    session = Session(aws_access_key_id=S3_ACCESS_KEY,
                      aws_secret_access_key=S3_SECRET_KEY)
    s3 = session.resource('s3')
    bucket = s3.Bucket('your_bucket')
    serializer_class = FileSerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk = self.kwargs['pk']
        )
        return obj

    @login_required(login_url='/login/')
    def home(request):
        """Principal view files"""
        for s3_file in bucket.objects.all():
            print(s3_file.key)
        if request.method == 'POST':
            form = FileForm(request.POST, request.FILES)
            data = form.cleaned_data
            f = open(data['document'], 'rb')
            if form.is_valid():
                form.save()
                #upload file
                s3_client = boto3.client('s3')
                try:
                    response = s3_client.upload_file(data['document'],bucket)
                except ClientError as e:
                    logging.error(e)
                    return False
                return redirect('/files/home')
        else:
            form = FileForm()
        return render(request, 'files.html',{'form': form, 'files': files})


    @login_required(login_url='/login/')
    def view_file(request, id_file):
        """Can view a file content"""
        file = File.objects.get(pk=id_file)
        name_file = file.document
        DataFrame = pd.read_csv(name_file)
        df = pd.DataFrame(DataFrame)
        return HttpResponse(df.to_html())


    @login_required(login_url='/login/')
    def delete_file(request, id_file):
        """Can view a file content"""
        file = File.objects.get(pk=id_file)
        file.delete()
        return redirect('/files/home')

