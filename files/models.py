from django.db import models


class File(models.Model):
    document = models.FileField(upload_to="files/")
    uploaded_at = models.DateTimeField(auto_now_add=True)

