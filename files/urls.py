from django.conf.urls import url
from django.urls import path
from . import views
from files.views import *

app_name = 'files'

urlpatterns = [
    #principal view files
    url(r'^home', FilesList.as_view() , name='home'),

]