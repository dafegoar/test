from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'users'

urlpatterns = [
    #principal view users
    url(r'^home', views.home, name='home'),
    #new user view
    url(r'^new_user', views.create_user, name='create_user'),
    #edit user view
    url('edit_user/(?P<id_user>[0-9]+)/$', views.edit_user, name='edit_user'),
    #delete user view
    url('delete_user/(?P<id_user>[0-9]+)/$', views.delete_user, name='delete_user'),

]