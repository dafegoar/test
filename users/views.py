from django.shortcuts import render, redirect
from users.forms import CustomUserCreationForm, CustomUserChangeForm
from users.models import *
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required(login_url='/login/')
def home(request):
    """Principal view"""
    users = CustomUser.objects.all()
    return render(request, 'users.html', {'users':users} )


@login_required(login_url='/login/')
def create_user(request):
    """Create new user"""
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.save()
            return redirect('/users/home')
        else:
            return render(request, 'create_user.html', {'form': form})
    else:
        form = CustomUserCreationForm()
        return render(request, 'create_user.html', {'form': form})


@login_required(login_url='/login/')
def edit_user(request, id_user):
    """Can edit a user"""
    user = CustomUser.objects.get(pk=id_user)
    if request.method == 'GET':
        form = CustomUserChangeForm(instance=user)
    else:
        form = CustomUserChangeForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('/users/home')
    return render(request, 'edit_user.html', {'form': form})


@login_required(login_url='/login/')
def delete_user(request, id_user):
    """Can delete a user"""
    user = CustomUser.objects.get(pk=id_user)
    user.delete()
    return redirect('/users/home')

