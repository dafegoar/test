from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        exclude = ['id','last_login','username' ,'is_staff', 'is_superuser','user_permissions','is_active','date_joined']
        fields = ['first_name', 'last_name', 'email','password1']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'password1': forms.PasswordInput(attrs={'class': 'form-control'})
        }
        labels = {
            'first_name': ('Nombre(s)'),
            'last_name': ('Apellidos'),
            'email': ('e-mail'),
            'password1': ('Contraseña'),
        }
        help_texts = {
            'username': '',
            'is_active': '',
            'is_staff': '',
            'groups': '',
        }
    def clean_email(self):
        data = self.cleaned_data['email']
        return data.lower()


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        exclude = ['id', 'last_login', 'username', 'is_staff', 'is_superuser', 'user_permissions', 'is_active',
                   'date_joined', 'password1']
        fields = ['first_name', 'last_name', 'email']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'first_name': ('Nombre(s)'),
            'last_name': ('Apellidos'),
            'email': ('e-mail'),
        }
        help_texts = {
            'username': '',
            'is_active': '',
            'is_staff': '',
            'groups': '',
        }

    def clean_email(self):
        data = self.cleaned_data['email']
        return data.lower()

