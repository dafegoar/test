from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from rest_framework.authtoken import views

urlpatterns = [
    #path('admin/', admin.site.urls),
    #Login
	path('login/', auth_views.LoginView.as_view(template_name='registration/login.html'),name='login'),
	#Logout
	path('logout/', auth_views.LogoutView.as_view(template_name='registration/login.html'),name='logout'),
    # address base module
    path('base/', include('base.urls')),
    # address files module
    path('files/', include('files.urls')),
    # address users module
    path('users/', include('users.urls')),
]

urlpatterns += [
    url(r'^api/auth', include('rest_framework.urls', namespace='rest_framework'))
]
